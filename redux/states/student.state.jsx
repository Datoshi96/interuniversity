
import { createSlice } from "@reduxjs/toolkit";

export const StudentEmptyState= [
  {
    name: '',
    credits: 0,
    subjects:[]
  },
];

export const studentSlice = createSlice({
  name: "student",
  initialState: [],
  reducers: {
    createStudent: (state, action) => action.payload,
    addStudent(state, action) {
      state.push(action.payload);
    },
    modifyStudent(state, action) {
      const { name, subjects } = action.payload;
      const item = state.find(item => item.name === name);
      if (item) {
        item.subjects.push(subjects);
        item.credits=item.credits+3;
      }
    },
    deleteSubjectStudent(state, action) {
      const { name,index } = action.payload;
      const item = state.find(item => item.name === name);
      if (item) {
        item.subjects.splice(index, 1);
      }
    },
    resetStudent: () => StudentEmptyState,
  },
});

export const {
  createStudent,
  modifyStudent,
  resetStudent,
  addStudent,
  deleteSubjectStudent,
} = studentSlice.actions;

export default studentSlice.reducer;