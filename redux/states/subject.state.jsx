
import { createSlice } from "@reduxjs/toolkit";

export const SubjectEmptyState= [
  {
    id: 0,
    name: '',
    credits: 3,
  },
];

export const subjectSlice = createSlice({
  name: "subject",
  initialState: [],
  reducers: {
    createSubject: (state, action) => action.payload,
    modifySubject: (state, action) => ({ ...state, ...action.payload }),
    addSubject(state, action) {
      state.push(action.payload);
    },
    addNewSubject(state, action) {
      state.push(action.payload);
    },
    resetSubject: () => SubjectEmptyState,
  },
});

export const {
  createSubject,
  modifySubject,
  resetSubject,
  addSubject,
  addNewSubject,
} = subjectSlice.actions;

export default subjectSlice.reducer;