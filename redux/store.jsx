import { configureStore } from '@reduxjs/toolkit';
import { studentSlice } from './states/student.state';
import { subjectSlice } from './states/subject.state';
import subjectReducer from './states/subject.state';
import { userSlice } from './states/user.state';
import studentReducer from './states/student.state';
import { pageSlice } from './states/page.state';
import pageReducer from './states/page.state';
import userReducer from './states/user.state'

export const store = configureStore({
  reducer:{
    studentReducer,
    pageReducer,
    userReducer,
    subjectReducer,
  }
});

export const makeStore = () => {
  return configureStore({
    reducer: {
      student: studentSlice.reducer,
      user: userSlice.reducer,
      page: pageSlice.reducer,
      subject: subjectSlice.reducer,
    }
  })
}