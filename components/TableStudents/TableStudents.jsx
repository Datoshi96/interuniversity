"use client";
import React, { useEffect, useState } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ImageIcon from "@mui/icons-material/Image";
import { Divider, Typography } from "@mui/material";
import styles from "@/styles/Home.module.css";
import { useAppSelector } from "@/redux/hooks";
import dataSubject from "@/utils/subjects.json";

const TableStudents = () => {
  const studentState = useAppSelector((state) => state.studentReducer);
  const userState = useAppSelector((state) => state.userReducer);

  const [students,  setStudents] = useState([]);

  useEffect(() => {
    if (studentState && studentState.length > 0) {
      let arraySub = [];
    studentState.forEach((stu) => {
      if (userState.user === stu.name && stu.subjects.length > 0) {
        stu.subjects.forEach((elem) => {
          let subject = dataSubject.filter((obj) => obj.id === elem.id);
          arraySub.push(subject[0]);
        });
      }
    });
    let studentsPar = [];
    studentState.forEach((stu) => {
      if (userState.user != stu.name && stu.subjects.length > 0) {
        stu.subjects.forEach((elem) => {
          arraySub.forEach(elem2 => {
              if(elem.id == elem2.id){
                  studentsPar.push(stu);
              }
          });
        });
      }
    });
    setStudents(studentsPar);

  }
  }, [studentState,userState]);

  return (
    <>
      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        className={styles.listcontainer}
      >
        <Typography
              gutterBottom
              variant="subtitle2"
              component="div"
              className={styles.text1}
            >
              Compañeros en línea
            </Typography>
        {Array.isArray(students) && students.length > 0 && (
            students.map((st,i)=>{
                return (
                    <div key={i}>
                        <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                            <ImageIcon />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={st.name} secondary={"Conectado"} />
                        </ListItem>
                        <Divider component="li" />
                    </div>
                )
            })
        )}
      </List>
    </>
  );
};

export default TableStudents;
