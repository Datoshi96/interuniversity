"use client";
import React, { useEffect, useState } from "react";
import styles from "@/styles/Home.module.css";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Checkbox,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormHelperText,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import dataSubject from "@/utils/subjects.json";
import { createSubject } from "@/redux/states/subject.state";
import {
  createStudent,
  deleteSubjectStudent,
  modifyStudent,
} from "@/redux/states/student.state";
import { TableStudents } from "@/components";
import service from "@/services/service";
import { useRouter } from "next/navigation";

const URL = "https://api.frankfurter.app/latest?to=USD";

export default function Subjects() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const studentState = useAppSelector((state) => state.studentReducer);
  const userState = useAppSelector((state) => state.userReducer);

  const [subjects, setSubjects] = useState([]);
  const [convEur, setConvEur] = useState(0);
  const [states, setStates] = useState({
    openAddSubject: false,
    subject: 0,
    errorSubject: false,
    disabledAddDialog: true,
    credits: 0,
    usd:0,
    eur:0,
  });

  const getData = async() => {
    const response = await service.post(URL);
    const convEurs = response.data.rates.USD;
    setConvEur(convEurs)
    if (studentState && studentState.length > 0) {
      studentState.forEach((stu) => {
        if (userState.user === stu.name && stu.subjects.length > 0) {
          let arraySub = [];
          stu.subjects.forEach((elem) => {
            let subject = dataSubject.filter((obj) => obj.id === elem.id);
            arraySub.push(subject[0]);
            setStates({
              ...states,
              credits: stu.credits,
              usd: (stu.credits*150),
              eur: (stu.credits*150)/convEurs
            });
          });
          setSubjects(arraySub);
        }
      });
    }
  };

  const handleChangeSubject = (event) => {
    setStates({
      ...states,
      subject: event.target.value,
      errorSubject: false,
      disabledAddDialog: false,
    });
  };

  const handleClose = () => {
    setStates({
      ...states,
      openAddSubject: false,
    });
  };

  const handleAdd = () => {
    setStates({
      ...states,
      openAddSubject: true,
    });
  };

  const handleAddSubject = () => {
    let subject = dataSubject.filter((obj) => obj.id === states.subject);

    studentState.forEach((stu) => {
      if (userState.user === stu.name) {
        if (Array.isArray(stu.subjects) && stu.subjects.length === 0) {
          setSubjects(subject);
          setStates({
            ...states,
            openAddSubject: false,
            credits: 3,
            usd: 450,
            eur:450/convEur
          });
          dispatch(modifyStudent({ name: stu.name, subjects: subject[0] }));
        } else {
          const teacherExist = stu.subjects.some(
            (item) => item.nameteacher === subject[0].nameteacher
          );
          if (!teacherExist) {
            setSubjects([subject[0], ...subjects]);
            setStates({
              ...states,
              openAddSubject: false,
              credits: states.credits + 3,
              usd: (states.credits+3)*150,
              eur: ((states.credits+3)*150)/convEur
            });
            dispatch(modifyStudent({ name: stu.name, subjects: subject[0] }));
          } else {
            setStates({
              ...states,
              errorSubject: true,
            });
          }
        }
      }
    });
  };

  const handleDeleteSubject = async (id, index) => {
    const newStudents = subjects.filter((obj) => obj.id !== id);
    setSubjects(newStudents);
    dispatch(deleteSubjectStudent({ name: userState.user, index }));
    setStates({
      ...states,
      credits: states.credits - 3,
      usd: (states.credits-3)*150,
      eur: ((states.credits-3)*150)/convEur
    });
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if(!userState.permission){
      router.push('/error404');
    }
  }, [userState.permission]);

  return (
    <>
      <h2 className={styles.h2}>Mis Materias</h2>
      <Typography
        gutterBottom
        variant="subtitle2"
        component="div"
        className={styles.text2}
      >
        Nombre de usuario: {userState.user}
      </Typography>
      <Typography
        gutterBottom
        variant="subtitle2"
        component="div"
        className={styles.text2}
      >
        Creditos: {states.credits}
      </Typography>
      <Typography
        gutterBottom
        variant="subtitle2"
        component="div"
        className={styles.text2}
      >
        Valor en USD: ${states.usd}
      </Typography>
      <Typography
        gutterBottom
        variant="subtitle2"
        component="div"
        className={styles.text2}
      >
        Valor en EUR: ${states.eur.toFixed(3)}
      </Typography>
      <Button
        onClick={handleAdd}
        color="primary"
        size="small"
        variant="contained"
        sx={{ marginBottom: "10px" }}
        disabled={states.credits === 9 ? true : false}
      >
        Añadir Materia
      </Button>
      {states.credits === 9 && (
        <Typography
          gutterBottom
          variant="subtitle2"
          component="div"
          className={styles.text2}
        >
          Un estudiante no puede añadir mas de 3 materias
        </Typography>
      )}
      <Container className={styles.container}>
        <Container className={styles.container2}>
          {subjects && subjects.length > 0 ? (
            subjects.map((sub, i) => {
              return (
                <Card
                  key={sub.id}
                  sx={{
                    marginBottom: "8px",
                  }}
                  className={styles.card}
                >
                  <CardContent>
                    <Typography
                      sx={{ fontSize: 14, marginTop: "10px" }}
                      color="text.secondary"
                      gutterBottom
                    >
                      {sub.name}
                    </Typography>
                  </CardContent>
                  <CardContent>
                    <Typography
                      sx={{ fontSize: 14, marginTop: "10px" }}
                      color="text.secondary"
                      gutterBottom
                    >
                      Profesor: {sub.nameteacher}
                    </Typography>
                  </CardContent>
                  <CardContent>
                    <Typography
                      sx={{ fontSize: 14, marginTop: "10px" }}
                      color="text.secondary"
                      gutterBottom
                    >
                      Creditos:3
                    </Typography>
                  </CardContent>
                  <CardActions
                    // anchorOrigin={{
                    //   vertical: "top",
                    //   horizontal: "right",
                    // }}
                  >
                    <Tooltip title="Eliminar tarea" placement="top-start">
                      <IconButton
                        aria-label="delete"
                        onClick={() => handleDeleteSubject(sub.id, i)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </CardActions>
                </Card>
              );
            })
          ) : (
            <Typography
              gutterBottom
              variant="subtitle2"
              component="div"
              className={styles.text2}
            >
              Por el momento aun no tienes inscritas materias, te invitamos a
              añadir una.
            </Typography>
          )}
        </Container>
        {Array.isArray(studentState) && studentState.length > 0 && (
          <TableStudents></TableStudents>
        )}
      </Container>

      <Dialog
        fullWidth
        open={states.openAddSubject}
        onClose={handleClose}
        sx={{
          "& .MuiPaper-root": {
            background: "#fff",
          },
        }}
      >
        <DialogTitle>Añadir una materia</DialogTitle>
        <DialogContent>
          <FormControl
            fullWidth
            sx={{ marginTop: "10px" }}
            error={states.errorSubject}
          >
            <InputLabel id="select-subject-label">Materia</InputLabel>
            <Select
              labelId="select-subject-label"
              id="simple-select"
              value={states.subject}
              label="materia"
              onChange={handleChangeSubject}
            >
              {dataSubject.map((subselect) => {
                return (
                  <MenuItem key={subselect.id} value={subselect.id}>
                    {subselect.name}/Profesor:{subselect.nameteacher}
                  </MenuItem>
                );
              })}
              <MenuItem value={10}>Ten</MenuItem>
            </Select>
            {states.errorSubject && (
              <FormHelperText>
                No es posible ver más de una clase con el mismo profesor
              </FormHelperText>
            )}
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleClose}>
            Cancelar
          </Button>
          <Button
            disabled={states.disabledAddDialog}
            color="primary"
            onClick={handleAddSubject}
          >
            Añadir
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
