"use client";
import { Container } from "@mui/material";

export default function Error404() {
    return (
        <Container>
        <h1>La pagina a la que intentas ingresar no se encuentra disponible</h1>
        </Container>
    )
};
