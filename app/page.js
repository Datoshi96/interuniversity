"use client";
import Image from "next/image";
import {
  Backdrop,
  Button,
  Card,
  CardContent,
  CircularProgress,
  TextField,
  Typography,
} from "@mui/material";
import styles from "@/styles/Home.module.css";
import React, { useEffect, useState, useContext, useRef } from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { createUser } from "@/redux/states/user.state";
import { useRouter } from "next/navigation";
import { addStudent, createStudent } from "@/redux/states/student.state";

export default function Home() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const studentState = useAppSelector((state) => state.studentReducer);
  const [states, setStates] = useState({
    errorMs: "",
    userName: "",
    password: "",
    buttonDisabled: true,
    open: false,
  });

  const handleChangeInputs = (e, type) => {
    if (type === "userName") {
      setStates({
        ...states,
        userName: e.target.value,
      });
    }
    if (type === "password") {
      setStates({
        ...states,
        password: e.target.value,
      });
    }
  };

  const handleLogin = async () => {
    setStates({
      ...states,
      open: true,
    });
    try {
      const newUserState = {
        user: states.userName,
        password: states.password,
        permission: true,
      };
      dispatch(createUser(newUserState));
      if (Array.isArray(studentState)) {
        const studentExist = studentState.some(
          (item) => item.name === states.userName
        );
        if (!studentExist) {
          const student = {
            name: states.userName,
            credits: 0,
            subjects: [],
          };
          if (studentState.length == 0) {
            dispatch(createStudent([student]));
          } else {
            dispatch(addStudent(student));
          }
        }
      }
      router.push("/subjects");
      setStates({
        ...states,
        open: false,
      });
    } catch (error) {
      setStates({
        ...states,
        errorMs: "Usuario o contraseña invalidos",
        open: false,
      });
    }
  };

  useEffect(() => {
    if (states.userName !== "" && states.password !== "") {
      setStates({
        ...states,
        buttonDisabled: false,
      });
    } else {
      setStates({
        ...states,
        buttonDisabled: true,
      });
      const newUserState = {
        userName: "",
        password: "",
        permission: false,
      };
      dispatch(createUser(newUserState));
    }
  }, [states.userName, states.password]);

  return (
    <>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={states.open}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <div>
        <h1 className={styles.h1}>Bienvenido a Inter University</h1>
      </div>
      <Card
        sx={{
          maxWidth: 600,
          background: "#fff",
          border: "0.5px solid #283b7a",
        }}
      >
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            className={styles.text1}
          >
            Iniciar Sesión
          </Typography>
          {states.errorMs && (
            <Typography
              sx={{ color: "red", fontSize: "14px" }}
              gutterBottom
              component="div"
            >
              Usuario o contraseña incorrectos
            </Typography>
          )}
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="UserName"
            type="text"
            fullWidth
            color="primary"
            variant="outlined"
            value={states.userName}
            onChange={(e) => handleChangeInputs(e, "userName")}
            required
          />
          <TextField
            autoFocus
            margin="dense"
            id="pass"
            label="Contraseña"
            type="password"
            fullWidth
            color="primary"
            variant="outlined"
            value={states.password}
            onChange={(e) => handleChangeInputs(e, "password")}
            required
          />
          <Button
            color="primary"
            variant="outlined"
            disabled={states.buttonDisabled}
            onClick={handleLogin}
          >
            Iniciar sesión
          </Button>
        </CardContent>
      </Card>
    </>
  );
}
